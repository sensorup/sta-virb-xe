#!/bin/bash

set -e

# Determine path to VLC. Different for Mac version.
if [ -f /Applications/VLC.app/Contents/MacOS/VLC ]; then
  VLC="/Applications/VLC.app/Contents/MacOS/VLC -I rc"
else
  VLC=$(which vlc-wrapper)
fi

$VLC $1 --sout '#http{mux=asf,dst=:7000/}' -vvv
