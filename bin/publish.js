"use strict";
var FS           = require('fs');
var moment       = require('moment');
var Q            = require('q');
var Request      = require('../src/sensor_things/request');
var SensorThings = require('../src/sensor_things');

var argv = require('yargs')
  .usage('Usage: $0 <VIRB URL> <STA URL>')
  .demand(2)
  .argv;

var readFile = Q.nfbind(FS.readFile);
var req = new Request({ throttle: 250 });
var ST = new SensorThings({
  request: req,
  url:     argv._[1]
});

function pp(obj) {
  return JSON.stringify(obj, null, "  ");
}

// Assume collection at current time
var phenomenonTime = moment().format();
var datastreamService = ST.datastreamService();
var sensors = [];

function getSensorResult(name) {
  var sensor = sensors.find(function(item) { return item.name === name; });
  if (sensor === undefined) {
    return null;
  } else {
    return sensor.data;
  }
}

var reqBody = '{ "command": "sensors" }';

Q(req.query({
  body: reqBody,
  method: "POST",
  headers: {
    // Content-Length must be set manually, otherwise Request calculates it
    // wrong and the Virb returns a blank response.
    "Content-Length": reqBody.length
  },
  url: argv._[0]
}))
.then(function(response) {
  sensors = JSON.parse(response.body)["sensors"];
  return Q.all([
    { datastream: "sta-templates/datastream-accel-inst.json", sensorName: "InternalAccelG" },
    { datastream: "sta-templates/datastream-accel-x.json", sensorName: "InternalAccelX" },
    { datastream: "sta-templates/datastream-accel-y.json", sensorName: "InternalAccelY" },
    { datastream: "sta-templates/datastream-accel-z.json", sensorName: "InternalAccelZ" },
    { datastream: "sta-templates/datastream-gyro-x.json", sensorName: "InternalGyroX" },
    { datastream: "sta-templates/datastream-gyro-y.json", sensorName: "InternalGyroY" },
    { datastream: "sta-templates/datastream-gyro-z.json", sensorName: "InternalGyroZ" }
  ].map(function(obj) {
    // Get datastream description from template then lookup on SensorThings
    return Q.nfcall(FS.readFile, obj.datastream)
    .then(function(file) {
      var datastream = JSON.parse(file);
      return datastreamService.find({}, `description eq '${datastream.description}'`);
    })
    .then(function(remoteDatastream) {
      if (remoteDatastream === null) {
        console.log("Datastream could not be found for " + obj.datastream);
        return null;
      } else {
        // Create the observation for that datastream
        var path = remoteDatastream["Observations@iot.navigationLink"];
        var observationService = ST.observationService(path);

        return observationService.create({
          phenomenonTime: phenomenonTime,
          result: getSensorResult(obj.sensorName)
        }).then(function(remoteObservation) {
          console.log("Created: " + pp(remoteObservation));
          return remoteObservation;
        });
      }
    });
  }));
})
.catch(function(reason) {
  console.error("Oops!");
  console.error(reason);
  process.exit(1);
})
.then(function() {
  console.log("Finished!");
  console.log(" GET requests executed: " + req.getCounter()["GET"]);
  console.log("POST requests executed: " + req.getCounter()["POST"]);
})
.done();
