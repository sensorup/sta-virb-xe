"use strict";
// Merge Thing into STA, reusing existing instances if possible.
var colors       = require('colors/safe');
var FS           = require('fs');
var Q            = require('q');
var Request      = require('../src/sensor_things/request');
var SensorThings = require('../src/sensor_things');

var argv = require('yargs')
  .usage('Usage: $0 <STA URL> [options]')
  .demand(1)
  .option('o', {
    describe: 'Cached entity output directory',
    nargs: 1,
    required: false
  })
  .argv;

var readFile = Q.nfbind(FS.readFile);
var req = new Request({ throttle: 250 });
var ST = new SensorThings({
  request: req,
  url:     argv._[0]
});

function pp(obj) {
  return JSON.stringify(obj, null, "  ");
}

Q.nfcall(FS.readFile, "sta-templates/thing.json")
.then(function (text) {
  // Create Thing
  var thing = JSON.parse(text);
  var thingService = ST.thingService();

  return thingService.findOrCreate({
    description: thing.description,
    properties:  thing.properties
  }, `description eq '${thing.description}'`);
})
.then(function(remoteThing) {
  console.log("Remote Thing: " + pp(remoteThing));

  var remoteSensor = Q.nfcall(FS.readFile, "sta-templates/sensor.json")
  .then(function(text) {
    // Create Sensor
    var sensor = JSON.parse(text);
    var sensorService = ST.sensorService();

    return sensorService.findOrCreate({
      description: sensor.description,
      encodingType: sensor.encodingType,
      metadata: sensor.metadata
    }, `description eq '${sensor.description}'`);
  })
  .then(function(remoteSensor) {
    console.log("Remote Sensor: " + pp(remoteSensor));
    return remoteSensor;
  });

  return Q.all([remoteThing, remoteSensor]);
})
.spread(function(remoteThing, remoteSensor) {

  var remoteLocation = Q.nfcall(FS.readFile, "sta-templates/location.json")
  .then(function(text) {
    // Create Location
    var location = JSON.parse(text);
    var locationService = ST.locationService(remoteThing["Locations@iot.navigationLink"]);

    return locationService.findOrCreate({
      description: location.description,
      encodingType: location.encodingType,
      location:  location.location
    }, `description eq '${location.description}'`);
  })
  .then(function(remoteLocation) {
    console.log("Remote Location: " + pp(remoteLocation));
    return remoteLocation;
  });

  // Iterate over datastream files for upload to SensorThings.
  // Extract the ObservedProperty and findOrCreate that in SensorThings, then
  // do the findOrCreate for the datastream using the linked sensor and
  // observed property.
  var remoteDatastreams = Q.all([
    "sta-templates/datastream-accel-inst.json",
    "sta-templates/datastream-accel-x.json",
    "sta-templates/datastream-accel-y.json",
    "sta-templates/datastream-accel-z.json",
    "sta-templates/datastream-gyro-x.json",
    "sta-templates/datastream-gyro-y.json",
    "sta-templates/datastream-gyro-z.json",
    "sta-templates/datastream-http-server.json"
  ].map(function(path) {
    return Q.nfcall(FS.readFile, path)
    .then(function(text) {
      var datastream = JSON.parse(text);

      // Create Observed Property
      var observedProperty = datastream.ObservedProperty;
      var opService = ST.observedPropertyService();

      return opService.findOrCreate({
        description: observedProperty.description,
        definition: observedProperty.definition,
        name:  observedProperty.name
      }, `definition eq '${observedProperty.definition}'`)
      .then(function(remoteObservedProperty) {
        // Create Datastream
        var datastreamService = ST.datastreamService(remoteThing["Datastreams@iot.navigationLink"]);

        return datastreamService.findOrCreate({
          description: datastream.description,
          observationType: datastream.observationType,
          unitOfMeasurement: datastream.unitOfMeasurement,
          Sensor: {
            "@iot.id": remoteSensor["@iot.id"]
          },
          ObservedProperty: {
            "@iot.id": remoteObservedProperty["@iot.id"]
          }
        }, `description eq '${datastream.description}'`)
        .then(function(remoteDatastream) {
          console.log("Remote Datastream: " + pp(remoteDatastream));
          return remoteDatastream;
        });
      });
    });
  }));

  return Q.all([remoteLocation, remoteDatastreams]);
})
.catch(function(reason) {
  console.error(colors.red("Oops!"));
  console.error(colors.red(reason));
  process.exit(1);
})
.then(function() {
  console.log(colors.green("Finished!"));
  console.log(" GET requests executed: " + req.getCounter()["GET"]);
  console.log("POST requests executed: " + req.getCounter()["POST"]);
})
.done();
