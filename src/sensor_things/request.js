"use strict";
var Q = require('q');
var RateLimiter = require('limiter').RateLimiter;
var request = require('request');

class Request {
  constructor(options) {
    if (options === undefined) {
      options = {};
    }

    this.dryRun = (!!options.dryRun);
    this.throttle = options.throttle || 100; // at most 1 request every 100 ms
    this.limiter = new RateLimiter(1, this.throttle);

    this.counter = {
      "GET":  0,
      "POST": 0
    };
  }

  get(url, query) {
    if (url === undefined) {
      throw "url not defined";
    }

    if (query === undefined) {
      query = {};
    }

    return this.query({
      json:   true,
      method: "GET",
      qs:     query,
      url:    url
    });
  }

  getCounter() {
    return this.counter;
  }

  post(url, body) {
    if (url === undefined) {
      throw "url not defined";
    }

    if (body === undefined) {
      throw "cannot post without a request body";
    }

    if (this.dryRun) {
      console.log(`Dry run: Was going to POST to ${url} with: `);
      console.log(JSON.stringify(body, null, "  "));
      return body;
    } else {
      return this.query({
        body:   body,
        headers: {
          "Content-Type": "application/json"
        },
        json:   true,
        method: "POST",
        url:    url
      });
    }
  }

  query(options) {
    var deferred = Q.defer();

    if (options.method) {
      this.counter[options.method] = this.counter[options.method] + 1;
    }

    this._throttledRequest(options, function(error, res, body) {
      if (error) {
        deferred.reject(error);
      } else {
        deferred.resolve(res);
      }
    });

    return deferred.promise;
  }

  _throttledRequest() {
    var requestArgs = arguments;
    this.limiter.removeTokens(1, function() {
        request.apply(this, requestArgs);
    });
  }

}

module.exports = Request;
