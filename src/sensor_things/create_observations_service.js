"use strict";
var Q   = require('q');

// Manage interaction with CreateObservations resource for SensorThings.
class CreateObservationsService {
  constructor(options) {
    this.request = options.request;
    this.url     = options.url;

    if (options.log) {
      this.log = options.log;
    } else {
      this.log = function() {};
    }
  }

  createAll(observations, options) {
    var deferred = Q.defer();
    var datastream = options["Datastream"];
    var keys = ["phenomenonTime", "result"];

    if (observations.length === 0) {
      return [];
    }

    var requestBody = [
      {
        "Datastream": datastream,
        "components": keys,
        "dataArray@iot.count": observations.length,
        "dataArray": observations.map(function(observation) {
          return keys.map(function(key) {
            return observation[key];
          });
        })
      }
    ];

    this.request.post(this.url, requestBody)
    .then((response) => {
      var entityList = response.body;
      this.log(entityList);
      deferred.resolve(entityList);
    }, function(reason) {
      deferred.reject(new Error(reason));
    })
    .done();

    return deferred.promise;
  }
}

module.exports = CreateObservationsService;
